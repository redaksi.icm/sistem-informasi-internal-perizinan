<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PenanamanModalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Penanaman Modals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penanaman-modal-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Penanaman Modal', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_penanaman_modal',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
